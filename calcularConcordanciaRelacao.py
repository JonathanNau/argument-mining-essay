from Corpus.CorpusRelacao import similiridade
from Corpus import extracao

import logging

logging.getLogger().setLevel(logging.INFO)

BASE_CORPUS = 'Corpus/CorpusRelacao/anotador1/'
BASE_CORPUS1 = 'Corpus/CorpusRelacao/anotador2/'

REDACOES1 = extracao.run(BASE_CORPUS)
REDACOES2 = extracao.run(BASE_CORPUS1)
similiridade.calcular_similaridade_relacoes(REDACOES1[:50], REDACOES2[:50])
