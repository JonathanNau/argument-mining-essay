import numpy as np

def adicionar_resultado(resultados, resultado_split):
    resultados.resultados.append(resultado_split)
    resultados.acuracias.append(resultado_split['Acuracia'])
    resultados.precisoes.append(resultado_split['Precisão'])
    resultados.recalls.append(resultado_split['Recall'])
    resultados.valores_f.append(resultado_split['F1-score'])
    resultados.matriz_confusao.append(resultado_split['Matriz de Confusão'])
    return resultados

def imprimir_media_resultado_gerais(resultados, tipo):
    print('------ Resultado da classificação: ' + tipo + '------')
    print('Acuracia média de: %f' % np.average(resultados.acuracias))
    print('Precisão média de: %f' % np.average(resultados.precisoes))
    print('Recall médio de: %f' % np.average(resultados.recalls))
    print('F1-score médio de: %f' % np.average(resultados.valores_f))
    print('----------------')

def imprimir_matriz_confusao(resultados, tipo):
    print('-------- Matriz Confusão ' + tipo + ' --------')
    for idx, matriz in enumerate(resultados.matriz_confusao):
        print('Matriz '+str(idx)+': ', matriz)
    print('----------------')
