from pulp import *
import numpy as np
import pickle
from joblib import load
import numpy as np
from config import BASE_CLASSIFICADORES
import logging

sentencas=[]
n_sentences=0
const_r = 1/2
const_cr = 1/4
const_c = 1/4

classificador_componentes = None
classificador_relacao = None

def c(i, j):
    '''Verificar o tipo do argumento atribuido pelo classificador'''
    data = np.asarray(sentencas[j].getValoresDosRecursos(), dtype=np.float64)
    resultado = classificador_componentes.predict([data]) 
    if resultado == 1:
        return 1
    if resultado == 2:
        return 1
    return 0

def rel(i):
    '''Cálculo das relações de saída e entrada do argumento'''
    relin = 0
    relout = 0
    for idx, sent1 in enumerate(sentencas):
        if idx == i:
            continue

        data1 = np.asarray(sentencas[i].getValoresDosRecursos(), dtype=np.float64)
        data2 = np.asarray(sent1.getValoresDosRecursos(), dtype=np.float64)
        data = np.append(data1, data2)

        if classificador_relacao.predict([data]) == 1:
            relout += 1
            
    for idx, sent1 in enumerate(sentencas):
        if idx == i:
            continue
        
        data1 = np.asarray(sentencas[i].getValoresDosRecursos(), dtype=np.float64)
        data2 = np.asarray(sent1.getValoresDosRecursos(), dtype=np.float64)
        data = np.append(data2, data1)

        if classificador_relacao.predict([data]) == 1:
            relin += 1

    return relin, relout

def rel_total():
    '''Cálculo do total de relações na redação'''
    rel = 0
    for idx1, sent1 in enumerate(sentencas):
        for idx2, sent2 in enumerate(sentencas):
            if idx1 == idx2:
                continue
            data1 = np.asarray(sent1.getValoresDosRecursos(), dtype=np.float64)
            data2 = np.asarray(sent2.getValoresDosRecursos(), dtype=np.float64)
            data = np.append(data1, data2)

            if classificador_relacao.predict([data]) == 1:
                rel += 1
    return rel

def cs(i):
    '''Cálculo de CS para calcular o CR'''
    relin, relout = rel(i)
    return ((relin - relout + n_sentences - 1) / 
            (rel_total() + n_sentences - 1))

def cr(i, j):
    '''Cálculo do CR para o peso W'''
    return (cs(j) - cs(i))

def r_function(i,j):
    '''Verificação da relação para cálculo do peso'''
    data1 = np.asarray(sentencas[i].getValoresDosRecursos(), dtype=np.float64)
    data2 = np.asarray(sentencas[j].getValoresDosRecursos(), dtype=np.float64)
    data = np.append(data1, data2)
    resultado = classificador_relacao.predict([data])
    return resultado[0]

def w_func(i, j):
    '''Função para calcular o peso W para a Função Objetivo'''
    return (const_r * r_function(i,j)) + (const_cr * cr(i,j)) + (const_c * c(i,j))

def run(REDACOES, clf_componentes, clf_relacao, idx_test, split):
    logging.info('Iniciando programação Linear')
    global classificador_componentes
    global classificador_relacao
    classificador_componentes = clf_componentes
    classificador_relacao = clf_relacao
    #Carrega redações
    #REDACOES = pickle.load(open(BASE_CLASSIFICADORES+'redacoes-split-0.pkl', 'rb'))
    salvarResultado = []
    for red in idx_test:
        #Definição do problema
        prob = LpProblem("Argument Mining Essay", LpMaximize)
        idxSentencas = []
        #Remove sentenças que não foram atribuidas com rotulos.
        for idx, sent in enumerate(REDACOES[red].sentencas):
            data = np.asarray(sent.getValoresDosRecursos(), dtype=np.float64)
            resultado = classificador_componentes.predict([data])
            if resultado[0] == 0:
                continue
            sentencas.append(sent)
            idxSentencas.append(idx)
        
        global n_sentences
        n_sentences = len(sentencas)

        #Variavel para otimizar e encontrar as relações
        x = [
            [
                LpVariable(f"r({i},{j})", cat=LpBinary)
                for j in range(n_sentences)
            ]
            for i in range(n_sentences)
        ]

        #Adição de variavel B para controle de ciclos.
        b = [
            [
                LpVariable(f"b({i},{j})", cat=LpBinary)
                for j in range(n_sentences)
            ]
            for i in range(n_sentences)
        ]

        for i in range(n_sentences):
            for j in range(n_sentences):
                prob.addConstraint(b[i][j]-x[i][j] >= 0, f'eq. 5{i}_{j}')

        #Não permite a formação de ciclos
        for i in range(n_sentences):
            for j in range(n_sentences):
                for k in range(n_sentences):
                    prob.addConstraint(b[i][k] - b[i][j] - b[j][k] >= -1, f'eq. 6{i}_{j}_{k}')       

        #Peso W para a função objetivo
        w = np.zeros((n_sentences, n_sentences))
        for i in range(n_sentences):
            for j in range(n_sentences):
                w[i][j] = w_func(i, j)

        #Função Objetivo
        prob.setObjective(sum(
            x[i][j] * w[i][j]
            for i in range(n_sentences)
            for j in range(n_sentences)
        ))

        #Bloqueia que exista uma relação para a mesma sentença
        for ij in range(n_sentences):
            prob.addConstraint(x[ij][ij] == 0, f"Bloquear mesma sentença {ij}")
            prob.addConstraint(b[ij][ij] == 0, f"Bloquear mesma sentença de B {ij}")
            
        #Permite que as sentenças tenham apenas uma saída
        for i in range(n_sentences):
            prob.addConstraint(sum(x[i]) <= 1)

        #Permite apenas uma sentença sem nenhuma saída
        prob.addConstraint(sum(sum(i) for i in x) <= n_sentences - 1)

        prob.solve()
        salvarResultado.append((x, idxSentencas))
        for i in range(n_sentences):
            for j in range(n_sentences):
                print(x[i][j].varValue, end=" ")
            print()

    pickle.dump(salvarResultado, open(BASE_CLASSIFICADORES+'Resultados PLI-split-'+split+'.pkl', 'wb'))
