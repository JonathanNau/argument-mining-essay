import csv

def features_componente(nome, split, REDACOES):
    with open('features-componente-'+nome+'-SPLIT'+str(split)+'.csv', mode='w') as employee_file:
        employee_writer = csv.writer(employee_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        #print([i.identificador for i in REDACOES[0].sentencas[0].recursos])
        employee_writer.writerow([i.identificador for i in REDACOES[0].sentencas[0].recursos])
        for redacao in REDACOES:
            for sentenca in redacao.sentencas:
                employee_writer.writerow([i.valor for i in sentenca.recursos])