'''Módulo para funções de pré-processamento'''
import string
import nltk
from nltk import tokenize
from nltk.stem.snowball import SnowballStemmer

STEMMER = SnowballStemmer("portuguese")

def tokenizar_texto(texto):
    return tokenize.word_tokenize(texto, language='portuguese')

def remover_stopword(texto):
    stopwords = nltk.corpus.stopwords.words('portuguese')
    pontuacao = string.punctuation + '``' + '“' + '”'

    palavras = tokenizar_texto(texto.lower())
    texto_sem_stopwords = []
    for palavra in palavras:
        if palavra not in stopwords and palavra not in pontuacao:
            texto_sem_stopwords.append(palavra)

    return ' '.join(texto_sem_stopwords)

def remover_pontuacao(texto):
    pontuacao = string.punctuation + '``' + '“' + '”' + '–'

    palavras = tokenizar_texto(texto.lower())
    texto_sem_stopwords = []
    for palavra in palavras:
        if palavra not in pontuacao:
            texto_sem_stopwords.append(palavra)

    return ' '.join(texto_sem_stopwords)

def lematizar():
    pass

def stemming(texto):
    texto = tokenizar_texto(texto)
    texto = ' '.join([STEMMER.stem(palavra) for palavra in texto])
    return texto
