import logging
from Experimentos import classificacao

def run_componente(redacoes, train, split):
    resultado = classificacao.run(redacoes, train, str(split))
    return resultado

def run_relacao(redacoes, train, split):
    resultado = classificacao.run_relacao(redacoes, train, str(split))
    return resultado

def getClfComponente():
    return classificacao.getClfComponente()

def getClfRelacao():
    return classificacao.getClfRelacao()