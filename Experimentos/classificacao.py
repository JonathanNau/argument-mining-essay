#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.feature_selection import mutual_info_classif
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_selection import VarianceThreshold
from joblib import dump
from config import SELECAO_FEATURES, SALVAR_CLASSIFICADORES, BASE_CLASSIFICADORES

from bunch import Bunch
import numpy as np

clf_componente = None
clf_relacao = None

def dataset_train_test(dataset, tipo, split):
    clf = svm.SVC(kernel='linear')
    clf.fit(dataset.X_train, dataset.y_train)
    
    if SALVAR_CLASSIFICADORES:
        dump(clf, BASE_CLASSIFICADORES + tipo + '-' + split + '.joblib') 

    global clf_componente
    global clf_relacao
    if tipo == 'Componentes':
        clf_componente = clf
    else:
        clf_relacao = clf
    y_pred = clf.predict(dataset.X_test)
    y_true = dataset.y_test
    
    resultados = {
        'Acuracia': accuracy_score(y_true, y_pred),
        'Precisão classes': precision_score(y_true, y_pred, average=None),
        'Precisão': precision_score(y_true, y_pred, average='weighted'),
        'Recall classes': recall_score(y_true, y_pred, average=None),
        'Recall': recall_score(y_true, y_pred, average='weighted'),
        'F1-score': f1_score(y_true, y_pred, average='weighted'),
        'Matriz de Confusão': confusion_matrix(y_true, y_pred)
    }
    return resultados

def selecao_features(dataset):
    data = dataset.X_train.copy()
    data.extend(dataset.X_test)
    sel = VarianceThreshold(threshold=(.8 * (1 - .8)))
    new_data = sel.fit_transform(data)
    dataset.X_train = new_data[:len(dataset.X_train)]
    dataset.X_test = new_data[len(dataset.X_train):]
    return dataset

def getClfComponente():
    global clf_componente
    if clf_componente is None:
        logging.info('Classificador de componente está nulo')
    return clf_componente

def getClfRelacao():
    global clf_relacao
    if clf_relacao is None:
        logging.info('Classificador de relação está nulo')
    return clf_relacao

def run(redacoes, train, split):
    dataset = Bunch(X_train=[],
                    X_test=[],
                    y_train=[],
                    y_test=[])

    for idx, redacao in enumerate(redacoes):
        for sentenca in redacao.sentencas:
            data = np.asarray(sentenca.getValoresDosRecursos(), dtype=np.float64)
            target = np.asarray(sentenca.getRotulo(), dtype=np.int)

            if idx in train:
                dataset.X_train.append(data)
                dataset.y_train.append(target)
            else:
                dataset.X_test.append(data)
                dataset.y_test.append(target)
    if SELECAO_FEATURES:
        dataset = selecao_features(dataset)
    return dataset_train_test(dataset, 'Componentes', split)

def run_relacao(redacoes, train, split):
    dataset = Bunch(X_train=[],
                    X_test=[],
                    y_train=[],
                    y_test=[])

    for idx, redacao in enumerate(redacoes):
        for idx_sent1, sentenca in enumerate(redacao.sentencas):
            lista_identificador = [sentenca.identificador == relacao.arg1 for relacao in redacao.relacao]
            for idx_sent2, sent in enumerate(redacao.sentencas):
                if idx_sent1 == idx_sent2:
                    continue

                data1 = np.asarray(sentenca.getValoresDosRecursos(), dtype=np.float64)
                data2 = np.asarray(sent.getValoresDosRecursos(), dtype=np.float64)
                data = np.append(data1, data2)
                
                if True in lista_identificador:
                    lista_identificador2 = [sentenca.identificador == relacao.arg1 and sent.identificador == relacao.arg2 for relacao in redacao.relacao]
                    if True in lista_identificador2:
                        target = np.asarray(1, dtype=np.int)
                    else:
                        target = np.asarray(0, dtype=np.int)

                else:
                    target = np.asarray(0, dtype=np.int)

                if idx in train:
                    dataset.X_train.append(data)
                    dataset.y_train.append(target)
                else:
                    dataset.X_test.append(data)
                    dataset.y_test.append(target)
    if SELECAO_FEATURES:
        dataset = selecao_features(dataset)
    return dataset_train_test(dataset, 'Relação', split)
