'''Módulo com paramêtros de configuração'''
import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

BASE_INDICADORES = os.path.join(ROOT_DIR, 'Utils/indicadores/')
BASE_LOG = os.path.join(ROOT_DIR, 'log/')
BASE_CLASSIFICADORES = os.path.join(ROOT_DIR, 'classificadores/')

EXTRACAO_CORPUS = False

SELECAO_FEATURES = False

EXECUTAR_EXPERIMENTO_COMPONENTE = True
EXECUTAR_EXPERIMENTO_RELACAO = True

EXPORT_FEATURES = False

BASE_CORPUS = 'Corpus/'

ESTATISTICA_CORPUS = False

SALVAR_CLASSIFICADORES = True
