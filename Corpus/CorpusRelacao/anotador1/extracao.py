#!/usr/bin/python
# -*- coding: utf-8 -*-

import pickle
import re

from nltk.tokenize import sent_tokenize

from Model.redacao import Redacao
from Model.marcacao import Marcacao
from Model.relacao import Relacao
from Model.sentenca import Sentenca
from Model.recurso import Recurso

import random

TIPOS_DE_ARGUMENTOS = ['Tese', 'Argumento', 'PI']

def read_arquivo(file, extensao, tipo_read):
    '''Abre o arquivo da redação e realiza a leitura'''
    arquivo = open(file + '.' + extensao, 'r', encoding='utf8')
    if tipo_read == 1:
        return arquivo.read()
    return arquivo.readlines()

def extrai_texto(file, redacao):
    '''realiza a extracao de dados do texto'''
    arquivo = read_arquivo(file, 'txt', 1).split('\n')
    redacao.titulo = arquivo[0]
    redacao.paragrafos = [re.sub(' +', ' ', i) for i in arquivo[1:]]
    redacao.texto = re.sub(' +', ' ', ' '.join(redacao.paragrafos))
  
    return redacao

def extrai_marcacao(file, redacao):
    '''realiza a extracao de dados das marcações'''
    arquivo = read_arquivo(file, 'ann', 2)

    marcacoes = []
    relacoes = []

    for linha in arquivo:
        linha = linha.split('\t')
        identificador = linha[0]
        texto = linha[2].rstrip("\n\r")
        linha = linha[1].split(' ')
        if linha[0] in TIPOS_DE_ARGUMENTOS:
            marcacoes.append(Marcacao(identificador, linha[0], linha[1], linha[2], texto))
        else:
            linha[1] = linha[1].replace('Arg1:', '')
            linha[2] = linha[2].replace('Arg2:', '')
            relacao = Relacao(linha[0], linha[1], linha[2])
            relacoes.append(relacao)
    redacao.marcacao = marcacoes
    redacao.relacao = relacoes
    return redacao

def get_nome_arquivo(numero):
    '''Recura nome do arquivo a ser lido'''
    return 'red'+f"{numero+1:02d}"

def extrai_sentencas(redacao):
    '''Realiza a extração de sentenças da redação'''
    for sentenca in sent_tokenize(redacao.texto, language='portuguese'):
        s = Sentenca(sentenca, '')
        '''for i in range(5):
        recurso = Recurso('F'+str(i), 'Texto F'+str(i), random.randint(1,9))
        s.recursos.append(recurso)'''
        redacao.sentencas.append(s)

def extrai_rotulos(redacao):
    '''Realiza a extração de rotulos de cada sentença'''
    for marcacao in redacao.marcacao:
        #Existe anotação com mais de uma sentença
        for j in marcacao.texto.split('.'):
            if j == '':
                continue
            for sentenca in redacao.sentencas:
                if j in sentenca.texto:
                    sentenca.rotulo = marcacao.nome

def run(file):
    '''Realiza a extrecao das marcações da redação'''
    redacoes = []

    for i in range(50):
        redacao = Redacao()
        extrai_texto(file + get_nome_arquivo(i), redacao)
        extrai_marcacao(file + get_nome_arquivo(i), redacao)
        extrai_sentencas(redacao)
        extrai_rotulos(redacao)

        redacoes.append(redacao)

    pickle.dump(redacoes, open("redacoes.pkl", "wb"))
    return redacoes
