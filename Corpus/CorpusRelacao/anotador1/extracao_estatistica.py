from bunch import Bunch

def run(redacoes):
    quantidade = Bunch(sentencas=0, palavras=0, teses=0, argumentos=0, pi=0)
    
    for redacao in redacoes:
        quantidade.sentencas += redacao.get_quantidade_de_sentencas()
        quantidade.palavras += redacao.get_quantidade_palavras()
        quantidade.teses += redacao.get_quantidade_do_rotulo('Tese')
        quantidade.argumentos += redacao.get_quantidade_do_rotulo('Argumento')
        quantidade.pi += redacao.get_quantidade_do_rotulo('PI')
        
    print('Quantidade de sentenças:', quantidade.sentencas)
    print('Quantidade de palavras:', quantidade.palavras)
    print('Quantidade de teses:', quantidade.teses)
    print('Quantidade de argumentos:', quantidade.argumentos)
    print('Quantidade de pi:', quantidade.pi)
