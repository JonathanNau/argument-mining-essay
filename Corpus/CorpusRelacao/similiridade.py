'''Calcular índice de concordância das relações'''
import logging
import krippendorff
import numpy as np

def calcular_similaridade_relacoes(REDACOES_ANOTADOR1, REDACOES_ANOTADOR2):
    tamanho_lista_redacoes_anotador1 = len(REDACOES_ANOTADOR1)
    tamanho_lista_redacoes_anotador2 = len(REDACOES_ANOTADOR2)
    if tamanho_lista_redacoes_anotador1 != tamanho_lista_redacoes_anotador2:
        logging.critical('Tamanho das redações são diferentes!')
        return
    resultados = []
    for i in range(tamanho_lista_redacoes_anotador1):
        logging.info('Criando vetores Redação %d', i)
        vetores = criar_vetores(REDACOES_ANOTADOR1[i], REDACOES_ANOTADOR2[i])       
        resultados.append(calcular_similaridade_krippendorf(vetores))
        logging.info('Concordância entre os avaliadores: %.2f', resultados[i])
    logging.info('Concordância Total entre os avaliadores: %.4f', np.mean(np.array(resultados)))

def criar_vetores(REDACAO1, REDACAO2):
    tamanho_lista = quantidade_de_possibilidades_de_relacoes(REDACAO1)## nome não está bom
    lista_redacao1 = np.zeros(tamanho_lista)
    lista_redacao2 = np.zeros(tamanho_lista)

    quantidade_marcacao = REDACAO1.get_quantidade_de_marcacao()
    posicoes = obter_posicao_de_insercao(quantidade_marcacao)

    lista_vetores_relacoes = []

    lista_vetores_relacoes.append(marcar_vetores(REDACAO1, lista_redacao1, posicoes))
    lista_vetores_relacoes.append(marcar_vetores(REDACAO2, lista_redacao2, posicoes))

    return lista_vetores_relacoes

def quantidade_de_possibilidades_de_relacoes(REDACAO1):
    total_marcacoes = len(REDACAO1.marcacao)
    quantidade_relacoes = total_marcacoes* (total_marcacoes-1)
    return quantidade_relacoes

def obter_posicao_de_insercao(tamanho_lista):
    #gerar posicoes
    dicionario_posicoes = {}
    posicao = 0
    for i in range(tamanho_lista):
        for j in range(tamanho_lista):
            if i != j:
                dicionario_posicoes['T'+str(i+1)+' -> T'+str(j+1)] = posicao
                posicao += 1

    return dicionario_posicoes

def marcar_vetores(REDACAO, lista_redacao, posicoes):
    for relacao in REDACAO.relacao:
        palavra = str(relacao.arg1) + ' -> ' + str(relacao.arg2)
        posicao = posicoes[palavra]
        lista_redacao[posicao] = 1

    return lista_redacao

def calcular_similaridade_krippendorf(vetores):
    krippendorffNominal = krippendorff.alpha(reliability_data=vetores, level_of_measurement='nominal')
    if np.isnan(krippendorffNominal):
        return 1
    return krippendorffNominal
