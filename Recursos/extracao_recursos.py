#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging

from Recursos import estruturais
from Recursos import lexicais
from Recursos import indicadores
from Recursos import sintaticos
from Recursos import contexto
from Recursos import word_embedding

def run(redacoes, train, test):
    logging.info('Iniciando a extração de recursos')
    lexicais.run(redacoes, train)
    estruturais.run(redacoes)
    indicadores.run(redacoes)
    sintaticos.run(redacoes)
    contexto.run(redacoes)
    word_embedding.run(redacoes)
    return redacoes
