'''Módulo para extração das features de indicadores'''
import pickle
import logging
import numpy as np
from Model.recurso import Recurso
from Utils.preprecessamento import remover_pontuacao, tokenizar_texto

MODEL = pickle.load(open('Utils/model300.pkl', 'rb'))


def calcular_word_embeddings(sentenca):
    texto = remover_pontuacao(sentenca.texto.lower())
    palavras = tokenizar_texto(texto)
    vetor_embedding = []
    for palavra in palavras:
        try:
            vetor_embedding.append(MODEL.get_vector(palavra))
        except:
            continue
    soma_vetores = np.sum(vetor_embedding, axis=0)
    for idx in range(len(soma_vetores)):
        recurso = Recurso('WD_'+str(idx),
                          'Valor do vetor na posição ' + str(idx),
                          soma_vetores[idx])
        sentenca.recursos.append(recurso)


def run(redacoes):
    logging.info('Iniciando a extração de recursos word embedding')
    for redacao in redacoes:
        for sentenca in redacao.sentencas:
            calcular_word_embeddings(sentenca)
    return redacoes