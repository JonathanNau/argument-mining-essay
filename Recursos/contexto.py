'''Módulo para extração das features de contexto'''
import logging
from config import BASE_INDICADORES
from Model.recurso import Recurso
from Model.sentenca import Sentenca
from Utils.preprecessamento import tokenizar_texto

LISTA_INDICADORES = {}

def extrair_lista_indicadores():
    LISTA_INDICADORES['forward'] = extrair_lista('forward')
    LISTA_INDICADORES['backward'] = extrair_lista('backward')
    LISTA_INDICADORES['tese'] = extrair_lista('tese')
    LISTA_INDICADORES['refutacao'] = extrair_lista('refutacao')

def extrair_lista(nome):
    arquivo = open(BASE_INDICADORES + nome + '.txt', 'r')
    lista = [linha.lower().rstrip() for linha in arquivo.readlines()]
    return lista

def extrair_indicadores_das_sentencas(sentenca, sentenca_referencia, prefix):
    for indicador in LISTA_INDICADORES:
        for token in LISTA_INDICADORES[indicador]:
            texto = sentenca_referencia.texto.lower()
            if len(token.split()) > 1:
                quantidade = texto.count(token)
            else:
                quantidade = tokenizar_texto(texto).count(token)
            if quantidade > 0:
                recurso = Recurso('CTX_'+token.upper()+prefix,
                              'Indicador (' + token.upper() + ') '+prefix+' da sentença',
                              0)
            else:
                recurso = Recurso('CTX_'+token.upper()+prefix,
                              'Quantidade do token (' + token.upper() + ') '+prefix+' da sentença',
                              1)
            
            sentenca.recursos.append(recurso)

def run(redacoes):
    logging.info('Iniciando a extração de recursos de contexto')
    extrair_lista_indicadores()
    for redacao in redacoes:
        for idx, sentenca in enumerate(redacao.sentencas):
            if idx == 0:
                extrair_indicadores_das_sentencas(sentenca, Sentenca("", "", ""), 'anterior')
                extrair_indicadores_das_sentencas(sentenca, redacao.sentencas[idx+1], 'posterior')
            elif idx == len(redacao.sentencas)-1:
                extrair_indicadores_das_sentencas(sentenca, redacao.sentencas[idx-1], 'anterior')
                extrair_indicadores_das_sentencas(sentenca, Sentenca("", "", ""), 'posterior')
            else:
                extrair_indicadores_das_sentencas(sentenca, redacao.sentencas[idx-1], 'anterior')
                extrair_indicadores_das_sentencas(sentenca, redacao.sentencas[idx+1], 'posterior')

    return redacoes
