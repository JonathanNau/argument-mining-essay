'''Módulo para extração das features de indicadores'''
import logging
from config import BASE_INDICADORES
from Model.recurso import Recurso
from Utils.preprecessamento import tokenizar_texto

LISTA_INDICADORES = {}

def extrair_lista_indicadores():
    LISTA_INDICADORES['forward'] = extrair_lista('forward')
    LISTA_INDICADORES['backward'] = extrair_lista('backward')
    LISTA_INDICADORES['tese'] = extrair_lista('tese')
    LISTA_INDICADORES['refutacao'] = extrair_lista('refutacao')

def extrair_lista(nome):
    arquivo = open(BASE_INDICADORES + nome + '.txt', 'r')
    lista = [linha.lower().rstrip() for linha in arquivo.readlines()]
    return lista

def extrair_indicadores_das_sentencas(sentenca):
    for indicador in LISTA_INDICADORES:
        for token in LISTA_INDICADORES[indicador]:
            texto = sentenca.texto.lower()
            if len(token.split()) > 1:
                quantidade = texto.count(token.lower())
            else:
                quantidade = tokenizar_texto(texto).count(token.lower())
            recurso = Recurso('IND_'+token.upper(),
                              'Quantidade do token (' + token.upper() + ') encontrados na sentença',
                              quantidade)
            sentenca.recursos.append(recurso)

def run(redacoes):
    logging.info('Iniciando a extração de recursos indicadores')
    extrair_lista_indicadores()
    for redacao in redacoes:
        for sentenca in redacao.sentencas:
            extrair_indicadores_das_sentencas(sentenca)
    return redacoes
