import logging
from nltk import tokenize
from Model.recurso import Recurso

def quantidade_de_caracteres(sentenca):
    quantidade = len(sentenca.texto)
    recurso = Recurso('EST001','Quantidade de caracteres na sentença', quantidade)
    sentenca.recursos.append(recurso)

def quantidade_de_palavras(sentenca):
    palavras_tokenize = tokenize.word_tokenize(sentenca.texto, language='portuguese') 
    quantidade = len(palavras_tokenize)
    recurso = Recurso('EST002','Quantidade de caracteres na sentença', quantidade)
    sentenca.recursos.append(recurso)

def quantidade_de_palavras_antecedentes(redacao, sentenca):
    index = redacao.sentencas.index(sentenca)
    quantidade = 0

    if index != 0:
        palavras_tokenize = tokenize.word_tokenize(' '.join([i.texto for i in redacao.sentencas[0:index]]), language='portuguese') 
        quantidade = len(palavras_tokenize)

    recurso = Recurso('EST003','Quantidade de tokens antecedentes', quantidade)
    sentenca.recursos.append(recurso)

def quantidade_de_palavras_precedentes(redacao, sentenca):
    index = redacao.sentencas.index(sentenca)
    ultimo_index = len(redacao.sentencas)-1
    quantidade = 0

    if index != ultimo_index:
        palavras_tokenize = tokenize.word_tokenize(' '.join([i.texto for i in redacao.sentencas[index:ultimo_index]]), language='portuguese') 
        quantidade = len(palavras_tokenize)

    recurso = Recurso('EST004','Quantidade de tokens precedentes', quantidade)
    sentenca.recursos.append(recurso)

def media_de_palavras_cobrindo_o_paragrafo(redacao, sentenca):
    quantidade_palavras_paragrafo = 0
    for paragrafo in redacao.paragrafos:
        if sentenca.texto in paragrafo:
            quantidade_palavras_paragrafo = len(tokenize.word_tokenize(paragrafo, language='portuguese'))
            break
    
    media = sentenca.getQuantidadePalavras() / quantidade_palavras_paragrafo
    recurso = Recurso('EST005','Média de tokens cobrindo o paragrafo da sentença', media)
    sentenca.recursos.append(recurso)


def esta_no_primeiro_paragrafo(redacao, sentenca):
    valor = 0
    if sentenca.texto in redacao.paragrafos[0]:
        valor = 1
    recurso = Recurso('EST006','Está no primeiro paragrafo', valor)    
    sentenca.recursos.append(recurso)

def esta_no_ultimo_paragrafo(redacao, sentenca):
    valor = 0
    #print(redacao.paragrafos[-1])
    if sentenca.texto in redacao.paragrafos[-2]:
        valor = 1
    recurso = Recurso('EST007','Está no último paragrafo', valor)    
    sentenca.recursos.append(recurso)

def run(redacoes):
    logging.info('Iniciando a extração de recursos estruturais')
    for redacao in redacoes:
        for sentenca in redacao.sentencas:
            quantidade_de_caracteres(sentenca)
            quantidade_de_palavras(sentenca)
            quantidade_de_palavras_antecedentes(redacao, sentenca)
            quantidade_de_palavras_precedentes(redacao, sentenca)
            media_de_palavras_cobrindo_o_paragrafo(redacao, sentenca)
            esta_no_primeiro_paragrafo(redacao, sentenca)
            esta_no_ultimo_paragrafo(redacao, sentenca)
    return redacoes
