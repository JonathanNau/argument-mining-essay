#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import spacy
import numpy as np

from cogroo_interface import Cogroo
from Model.recurso import Recurso

COGROO = Cogroo.Instance()
nlp = spacy.load('pt_core_news_sm')

def extrair_pos_tag(sentenca):
    pos_tag = COGROO._pos_tags()
    for pos in pos_tag:
        tokens = ' '.join([i.pos for i in sentenca.analise_cogroo.sentences[0].tokens])
        quantidade = tokens.count(pos)
        recurso = Recurso('SINT_'+pos.upper(),
                              'Quantidade do POS_TAG (' + pos.upper() + ') encontrados na sentença',
                              quantidade)
        sentenca.recursos.append(recurso)
    
def extrair_conjuncao(sentenca):
    tokens = ' '.join([i.pos for i in sentenca.analise_cogroo.sentences[0].tokens])
    conjuncoes = ['conj', 'conj-s', 'conj-c']
    quantidade = sum([tokens.count(conjuncao) for conjuncao in conjuncoes])
    recurso = Recurso('SINT_CONJ',
                      'Quantidade de conjunções encontrados na sentença',
                      quantidade)
    sentenca.recursos.append(recurso)

def extrair_verbos(sentenca):
    tokens = ' '.join([i.pos for i in sentenca.analise_cogroo.sentences[0].tokens])
    verbos = ['v', 'v-fin', 'v-inf', 'v-pcp', 'v-ger']
    quantidade = sum([tokens.count(verbo) for verbo in verbos])
    recurso = Recurso('SINT_VERB',
                      'Quantidade de verbos encontrados na sentença',
                      quantidade)
    sentenca.recursos.append(recurso)

def tree_height(root):
    if not list(root.children):
        return 1
    return 1 + max(tree_height(x) for x in root.children)

def extrair_profundidade_arvore(sentenca):
    doc = nlp(sentenca.texto)
    roots = [sent.root for sent in doc.sents]
    return np.mean([tree_height(root) for root in roots])

def run(redacoes):
    logging.info('Iniciando a extração de recursos sintáticos')
    for redacao in redacoes:
        for sentenca in redacao.sentencas:
            extrair_pos_tag(sentenca)
            extrair_conjuncao(sentenca)
            extrair_verbos(sentenca)
            extrair_profundidade_arvore(sentenca)
