'''Módulo para extração das features lexicais'''
from collections import Counter
import logging
import spacy
from Utils.preprecessamento import remover_stopword, stemming, tokenizar_texto
from Model.recurso import Recurso

NLP = spacy.load('pt_core_news_sm')

def extrair_unigramas(redacao):
    redacao = stemming(remover_stopword(redacao))
    return tokenizar_texto(redacao)

def extrair_pares_de_palavras_de_dependencia(redacao):
    for sentenca in redacao.sentencas:
        doc = NLP(stemming(remover_stopword(sentenca.texto)))
        for token in doc:
            if token.text == token.head.text:
                continue
            return token.text+'_'+token.head.text

def extrair_componentes(redacoes, train):
    LISTA_UNIGRAMAS = []
    LISTA_PALAVRAS_DEPENDENCIA = []
    for idx in train:
        LISTA_UNIGRAMAS.extend(extrair_unigramas(redacoes[idx].texto))
        LISTA_PALAVRAS_DEPENDENCIA.append(extrair_pares_de_palavras_de_dependencia(redacoes[idx]))
    LISTA_UNIGRAMAS = Counter(LISTA_UNIGRAMAS)
    LISTA_PALAVRAS_DEPENDENCIA = Counter(LISTA_PALAVRAS_DEPENDENCIA)
    return LISTA_UNIGRAMAS, LISTA_PALAVRAS_DEPENDENCIA

def extrair_unigramas_recrusos(sentenca, LISTA_UNIGRAMAS, LISTA_PALAVRAS_DEPENDENCIA):
    sentenca_sem_stopwords = stemming(remover_stopword(sentenca.texto))
    lista_palavras = tokenizar_texto(sentenca_sem_stopwords)
    for unigrama in LISTA_UNIGRAMAS:
        quantidade = lista_palavras.count(unigrama)
        recurso = Recurso('LEX_UNI_'+unigrama.upper(),
                          'Quantidade de unigrama '+unigrama.upper()+' na sentença',
                          quantidade)
        sentenca.recursos.append(recurso)

def extrair_palavras_dependencia(sentenca, LISTA_UNIGRAMAS, LISTA_PALAVRAS_DEPENDENCIA):
    sentenca_sem_stopwords = stemming(remover_stopword(sentenca.texto))
    doc = NLP(sentenca_sem_stopwords)
    dwp_sentenca = []
    for token in doc:
        if token.text == token.head.text:
            continue
        dwp_sentenca.append(token.text+'_'+token.head.text)

    for pares in LISTA_PALAVRAS_DEPENDENCIA:
        quantidade = dwp_sentenca.count(pares)
        recurso = Recurso('LEX_DWP_'+pares.upper(),
                          'Quantidade dos pares '+pares.upper()+' na sentença',
                          quantidade)
        sentenca.recursos.append(recurso)

def obter_mais_comuns(lista, maximo):
    nova_lista = []

    for palavra in lista.most_common(maximo):
        nova_lista.append(palavra[0])
    return nova_lista

def run(redacoes, train):
    logging.info('Iniciando a extração de recursos lexicais')
    LISTA_UNIGRAMAS, LISTA_PALAVRAS_DEPENDENCIA = extrair_componentes(redacoes, train)

    LISTA_UNIGRAMAS = obter_mais_comuns(LISTA_UNIGRAMAS, 4000)
    LISTA_PALAVRAS_DEPENDENCIA = obter_mais_comuns(LISTA_PALAVRAS_DEPENDENCIA, 4000)

    for redacao in redacoes:
        for sentenca in redacao.sentencas:
            extrair_unigramas_recrusos(sentenca, LISTA_UNIGRAMAS, LISTA_PALAVRAS_DEPENDENCIA)
            extrair_palavras_dependencia(sentenca, LISTA_UNIGRAMAS, LISTA_PALAVRAS_DEPENDENCIA)

    del LISTA_UNIGRAMAS
    del LISTA_PALAVRAS_DEPENDENCIA

    return redacoes
