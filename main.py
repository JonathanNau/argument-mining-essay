#!/usr/bin/python
# -*- coding: utf-8 -*-


import pickle
import logging
from datetime import datetime
from bunch import Bunch
from sklearn.model_selection import KFold
from Corpus import extracao
from Corpus import extracao_estatistica
from Recursos import extracao_recursos
from Experimentos import experimentos
import config
import exportar_files
import pli

import resultados


logging.getLogger().setLevel(logging.INFO)
logging.info('Iniciando Experimento.')

if config.EXTRACAO_CORPUS:
    REDACOES = extracao.run(config.BASE_CORPUS)
else:
    REDACOES = pickle.load(open('redacoes.pkl', 'rb'))
#REDACOES = REDACOES[:25]
if config.ESTATISTICA_CORPUS:
    extracao_estatistica.run(REDACOES)

RESULTADOS_COMPONENTE = Bunch(resultados=[], acuracias=[], precisoes=[], recalls=[], valores_f=[], matriz_confusao=[])
RESULTADOS_RELACAO = Bunch(resultados=[], acuracias=[], precisoes=[], recalls=[], valores_f=[], matriz_confusao=[])

K_FOLD = KFold(n_splits=5)
for idx, dataset in enumerate(K_FOLD.split(range(len(REDACOES)))):
    if config.SALVAR_CLASSIFICADORES:
        pickle.dump(dataset, open(config.BASE_CLASSIFICADORES+"dataset-split-"+str(idx), "wb"))
    
    train, test = dataset
    logging.info('Executando split: %d' % (idx + 1))
    redacoes = REDACOES
    extracao_recursos.run(redacoes, train, test)

    if config.EXPORT_FEATURES:
        logging.info('Exportando as features do split')
        data_atual = datetime.now().strftime("%d-%m-%Y %H:%m")
        exportar_files.features_componente(data_atual, idx, redacoes)

    if config.EXECUTAR_EXPERIMENTO_COMPONENTE:
        logging.info('Executando treinamento da classificação do componente')
        resultado_componente = experimentos.run_componente(redacoes, train, idx)
        RESULTADOS_COMPONENTE = resultados.adicionar_resultado(RESULTADOS_COMPONENTE, resultado_componente)
    
    if config.EXECUTAR_EXPERIMENTO_RELACAO:
        logging.info('Executando treinamento da classificação da relação')
        resultado_relacao = experimentos.run_relacao(redacoes, train, idx)
        RESULTADOS_RELACAO = resultados.adicionar_resultado(RESULTADOS_RELACAO, resultado_relacao)

    pli.run(redacoes, experimentos.getClfComponente(), experimentos.getClfRelacao(), test, idx+1)
    pickle.dump(redacoes, open(config.BASE_CLASSIFICADORES+"redacoes-split-"+str(idx)+".pkl", "wb"))

    del redacoes
    logging.info('Finalizado execução do split: %d' % (idx + 1))

logging.info('Finalizado execução dos experimentos!!')

data_atual = datetime.now().strftime("%d-%m-%Y %H:%m")
pickle.dump(RESULTADOS_COMPONENTE, open(config.BASE_LOG+'MEDIAS_COMPONENTE'+data_atual+'.pkl', 'wb'))
pickle.dump(RESULTADOS_RELACAO, open(config.BASE_LOG+'MEDIAS_RELACAO'+data_atual+'.pkl', 'wb'))

if config.EXECUTAR_EXPERIMENTO_COMPONENTE:
    logging.info('Resultados da classificação dos componentes')
    resultados.imprimir_media_resultado_gerais(RESULTADOS_COMPONENTE, 'Componentes')
    resultados.imprimir_matriz_confusao(RESULTADOS_COMPONENTE, 'Componentes')

if config.EXECUTAR_EXPERIMENTO_RELACAO:
    logging.info('Resultados da classificação das relações')
    resultados.imprimir_media_resultado_gerais(RESULTADOS_RELACAO, 'Relação')
    resultados.imprimir_matriz_confusao(RESULTADOS_RELACAO, 'Relação')
